'use strict'

const numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');

let personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
};

const movie1 = prompt('Один из последних просмотренных фильмов?', '');
const movie1Rating = +prompt('На сколько оцените его?', '');

const movie2 = prompt('Один из последних просмотренных фильмов?', '');
const movie2Rating = +prompt('На сколько оцените его?', '');

personalMovieDB.movies[movie1] = movie1Rating;
personalMovieDB.movies[movie2] = movie2Rating;
